---
layout: markdown_page
title: "JC Choi’s README"
---

**JC Choi** here. **Technical Instructional Designer** for **Professional Services**.

My intention is to let team members know why I love to collaborate, why I think the entire idea of GitLab is phenomenal, and why I try everyday to let my work life live CREDIT. If you think that's over-corn, update this.

### My one breath elevator pitch

I'm a dog person and even if you're not I know that there are many things about dogs you appreicate: loyalty their entire life, not overly attention-wanting but still attention-getting, and an amazing ability to be happy under all types of crappy situations. My ideals as a human walks on four legs and a wag.


### Why DIB

I was diagnosed really late in life. In my community mental health is a topic that is avoided. And so for a period I found myself in those places we could never see ourselves. Homeless. Addicted. Jail. Isolated. So now I look to build alliance and community with people who need help, a hand up. When I found out about the Handbook and came across DIB, I reached up as high as I could and pulled on the universe's ear and whispered a few words that got me here.


### Things I continue to do since my 20s

* Enter the Wu-Tang - 36 Chambers
* Pasquale's House Special, extra garlic, anchovies on the side
* Learning languages
* Korean food, cooking and eating
* Breaking tech


### Why I always lose Trivia Night

+ 7 dogs and 2 cats. I am a duck and goat away from being a petting zoo.
+ Drove for Uber and made food for DoorDash out of my house and tried dropshipping.
+ My MA dissertation was on remote/blended learning environments, which is what's happening in schools right now.
+ My annual month in Japan is how I disconnect, eat, and write. 
+ Built an indoor hydroponic garden system using two book cases, GroRocks, 6 inch plastic pots, a high wattage light, a timer, a fish tank water pump, and carbon dioxide snaked all around the whole affair. In my mom's living room. Until the downstair's neighbor complained about a growing water stain on their ceiling.


### You need me? Reach out. Anyway you want.

+ Slack, email, comments in GitLab, text, chat in Zoom, etc.
+ My personal SLA is 5-10 minutes. I know I know. We are asynchronous. Five to ten minutes is asynchronous, right.
+ People before process is real to me.


### What I need from you

+ Be as transparent as possible, but not more than you are comfortable.
+ Be authentic. I have a high bs meter.
+ Be friendly because why the heck not.


### Written ideas on training

<!-- TO ITERATE: add links to personal blog posts -->

Published a couple articles on LinkedIn:
+ https://www.linkedin.com/pulse/3-dumb-things-make-your-technical-training-delivery-better-jc-choi/
+ https://www.linkedin.com/pulse/learner-engagement-myth-jc-choi/


