---
layout: handbook-page-toc
title: "Product Marketing Messaging"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What are the key customer problems?

Teams across the software delivery lifecycle function struggle with:

* Usage of different tools preferred by each team or individual
* Multiple integrations across the tools to make the lifecycle work
* Multiple configuration challenges
* Different work processes across teams
* Lack of common metrics across teams to measure improvements
* Sequential flow of processes and handoffs that are slow, error prone, and brittle

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production teams need to manually approve releases
* legal/compliance sign offs are required
* Long testing cycles
* Separate build, QA, security, governance, and operations teams working in silos
* Hard to diagnose and time consuming to fix production failures

## GitLab value proposition
How does GitLab help solve the customer problems?

### Short single sentence

GitLab is a complete DevOps platform, delivered as a single application.

### Single sentence

GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate.


### Short message (~50 words)

GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity.


### Medium message (~250 words)

GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect.

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle. For the first time, Product, Development, QA, Security, and Operations teams can collaborate in a single application. There’s no need to integrate and synchronize tools, or waste time waiting for handoffs. Everyone contributes to a single conversation, instead of managing multiple threads across disparate tools. Development teams have complete visibility across the lifecycle with a single, trusted source of data to simplify troubleshooting and drive accountability. All activity is governed by consistent controls, making security and compliance first-class citizens instead of an afterthought.

Built on Open Source, GitLab works alongside its community of thousands of contributors and millions of users to continuously deliver new DevOps innovations. An estimated 30 million users trust GitLab to deliver great software at new speeds from startups, to leading open source projects, to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, Drupal, NASDAQ, Dish Network, and Comcast.

### Long message (~450 Words)

GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect. Now, fast paced teams no longer have to integrate or synchronize multiple DevOps tools and are able to go faster by working seamlessly across the complete lifecycle.

GitLab delivers complete real-time visibility of all projects and relevant activities across the entire DevOps lifecycle. For the first time, teams can see everything that matters. Changes, status, cycle times, security and operational health are instantly available from a trusted single source of data. Information is shown where it matters most, e.g. production impact is shown together with the code changes that caused it. And developers see all relevant security and ops information for any change. With GitLab, there is never any need to wait on synchronizing your monitoring app to version control or copying information from tool to tool. GitLab frees teams to manage projects, not tools. These powerful capabilities eliminate guesswork, help teams drive accountability and gives everyone the data-driven confidence to act with new certainty. With GitLab, DevOps teams get better every day by having the visibility to see progress and operate with a deeper understanding of cycle times across projects and activities.

GitLab drives radically faster cycle times by helping DevOps teams achieve higher levels of efficiency across all stages of the lifecycle making it possible for Product, Development, QA, Security, and Operations teams to work at the same time, instead of waiting for handoffs. Teams can collaborate and review changes together before pushing to production. GitLab eliminates the need to manually configure and integrate multiple tools for each project. GitLab makes it easy for teams to get started, they can start with GitLab on the one or two use cases they need to improve, and then begin their evolution to a single end-to-end experience for all of DevSecOps.

Only GitLab delivers DevOps teams powerful new governance capabilities embedded across the expanded lifecycle to automate security, code quality and vulnerability management. With GitLab, tighter governance and control never slow down DevOps speed.

GitLab leads the next advancement of DevOps. Built on Open Source, GitLab  delivers new innovations and features on the same day of every month by working alongside its community of thousands of passionate, global contributors and millions of users. An estimated 30 million users from the world’s most demanding organizations trust GitLab to deliver great software at new speeds.

### Company overview text
The [company overview](/company/#about-us) can be found here.

### Press release boilerplate
The [press release boilerplate](/press/press-kit/#boilerplate) can be found here.

### Standard Email introduction text

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an integrated product for the entire software development lifecycle. GitLab is fundamentally changing the way Development, Security, and Ops teams collaborate. GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity. An estimated 30 million users across the globe trust GitLab to deliver great software at new speeds.

## Key messaging guidelines

### GitLab is a product and a platform

GitLab is both a product and a platform. It is a product in that it is a single application that you can buy and use. It is a platform in that it enables multiple use cases, and is a framework on which to build an unlimited number of additional use cases (because most functions are available through an API), supported by an [ecosystem enablement group](/direction/create/ecosystem/). Users have a fully functional product out-of-the-box, but they are not limited in extending and integrating with other systems. Other examples of products that are also platforms are: the iPhone (which didn't even have an app store when it launched) and Facebook.

While some more traditional definitions of platforms are [more limited](http://www.dictionary.com/browse/software-platform), there are other perspectives that take a broader view of the definition of platform. Specifically, Adrian Bridgewater explains the Facebook example in [this article](https://www.forbes.com/sites/adrianbridgwater/2015/03/17/whats-the-difference-between-a-software-product-and-a-platform/#5042c24056a6) and also quotes blogger Jonathan Clarks who says, "Platforms are structures that allow multiple products to be built within the same technical framework" - which, one could argue, describes GitLab very well. [Another article](https://medium.com/platform-hunt/the-8-types-of-software-platforms-473c74f4536a) by Platform Hunt, a blog that studies platform business models, outlines 9 types of software platforms. GitLab most closely resembles an interaction platform, where identity is the foundational characteristic and the platform facilitates digital interactions. In GitLab's case, those digital interactions are collaborative coding, facilitated through both MRs and Issues.

Referring to GitLab as a platform, delivered as a single application, does nothing to diminish the key competitive differentiation of GitLab, as a single application approach has [many benefits](/handbook/product/single-application).  Referring to GitLab as a platform also increases the potential TAM (total available market) for GitLab, and decreases potential confusion, as some might think that 'single application' refers to SCM only.

### Minimize Git and Git-Only related concepts when talking about the company

We want to focus on the 'complete DevOps platform, delivered as a single application' message. Since "Git" is in our company name, we do not need to re-enforce that we are a Git-based SCM in our messaging. We should always default to the 'complete DevOps platform, delivered as a single application' messaging, and, at a minimum, message multiple stage solutions.

### Headline copy is different than messaging

Sometimes, we will use headlines with the purpose of grabbing someone's attention on a billboard, banner ad or blog post. Headlines can be more experimental, and sometimes use different words than official messaging.  This is acceptable, as long as they are truthful and authentic. For example, our advertising headlines might be different than our value driver language. This is an appropriate use of different messaging for the purpose of engagement and experimentation.

### Messaging to avoid

1. Don't use terms like “first” and “only”. They are extremely difficult to prove with evidence so they lack credibility. Additionally, our partners often have policies that explicitly forbid this type of messaging so whenever we do joint marketing (press release, event sponsorship, etc.) they ask us to change this wording.
2. Don't use the term "integrated". Yes, GitLab is an integrated product, but Atlassian can claim the same thing even though their product suite is made up of many different apps in different code bases with even the prem and cloud versions for the same app being written in different programming languages. Instead use, "single application", "built from the ground up", or "built in" to describe GitLab's integrated nature.  


### Pricing tier messaging

Go to the [Pricing tier page](/handbook/marketing/strategic-marketing/tiers/) to see [Tier Messaging](/handbook/marketing/strategic-marketing/tiers/#tier-messaging)

### GitLab positioning FAQs

Go to the [GitLab positioning FAQ page](/handbook/positioning-faq/).
